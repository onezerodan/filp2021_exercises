package exercises01

class Vector(val x: Double, val y: Double) {
  def +(other: Vector): Vector = new Vector(x + other.x, y + other.y)

  def -(other: Vector): Vector = new Vector(x - other.x, y - other.y)

  def *(scalar: Double): Vector = new Vector(x * scalar, y * scalar)

  def unary_- : Vector = new Vector(-x, -y)

  def euclideanLength: Double = (math.sqrt(x * x + y * y))

  def normalized: Vector = new Vector(x / math.sqrt(x * x + y * y), y / math.sqrt(x * x + y * y))

  override def equals(other: Any): Boolean =
    other match {
      case v: Vector => v.x == x && v.y == v.y
      case _         => false
    }

  // Vector(x, y)
  override def toString: String = new String(s"Vector($x, $y)")
}

object Vector {
  def fromAngle(angle: Double, length: Double): Vector = new Vector(length * math.cos(angle), length * math.sin(angle))

  def sum(list: List[Vector]): Vector = list.foldLeft(new Vector(0, 0))((res: Vector, vector) => res + vector)

  def unapply(arg: Vector): Option[(Double, Double)] = Some((arg.x, arg.y))

}
